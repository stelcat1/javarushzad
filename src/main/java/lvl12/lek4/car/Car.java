package lvl12.lek4.car;

public class Car {

    Engine engine = new Engine();

    class Engine {
        boolean isRunning;

        public void start() {
            this.isRunning = true;
            System.out.println("engine start");
        }

        public void stop() {
            this.isRunning = false;
            System.out.println("engine stop");
        }

        public boolean getEnginestatus() {
            if (!isRunning) {
                System.out.println("engine stop");
                return false;
            } else {
                System.out.println("engine start");
                return true;
            }


        }

    }


}
