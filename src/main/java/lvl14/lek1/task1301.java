package lvl14.lek1;

import java.util.Collections;
import java.util.HashSet;


/**
 * В классе Solution есть метод arrayToHashSet(String[]), который должен из переданного
 * массива вернуть HashSet<String> с теми же элементами.
 * Метод main не участвует в проверке.
 */


public class task1301 {
    public static void main(String[] args) {
        String[] array = {"Через", "три", "года", "я", "буду", "Senior", "Java", "Developer"};
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("___________________________________");

        HashSet<String> hashSet = arrayToHashSet(array);
        for (String s : hashSet) {
            System.out.println(s);
        }
    }


    public static HashSet<String> arrayToHashSet(String[] strings) {
        // создаем новый HashSet
        HashSet<String> hashSet = new HashSet<>();
        // добавляем все элементы массива в HashSet
        Collections.addAll(hashSet, strings);
        // возвращаем HashSet
        return hashSet;
    }
}