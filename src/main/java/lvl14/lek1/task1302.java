package lvl14.lek1;

import java.util.Arrays;
import java.util.HashSet;

import static java.util.Arrays.asList;

/**
 * В классе Solution есть метод checkWords(String), который должен проверять наличие переданного слова в множестве words.
 * Если слово есть, то выводим в консоль:
 * Слово [переданное слово] есть в множестве
 * Если нет:
 * Слова [переданное слово] нет в множестве
 * <p>
 * Метод main не участвует в проверке.
 * <p>
 * Пример вывода:
 * Слово Java есть в множестве
 */

public class task1302 {
    public static HashSet<String> words = new HashSet<>(asList("Если бы меня попросили выбрать язык на замену Java я бы не выбирал".split(" ")));

    public static void checkWords(String word) {
        if (word.equals(word)) {
            System.out.println("Слово " + word + " есть в множестве");
        } else {
            System.out.println("Слово " + word + " нет в множестве");
        }
    }

    public static void main(String[] args) {
        checkWords("JavaScript");
        checkWords("Java");
    }
}
