package lvl14.lek3;

import java.util.ArrayList;
import java.util.Collections;

public class task1308 {
    public static Integer min(ArrayList<Integer> list) {
        Integer min = Collections.min(list);
        return min;
    }

    public static Integer max(ArrayList<Integer> list) {
        Integer max = Collections.max(list);
        return max;
    }

    public static int frequency(ArrayList<Integer> list, Integer element) {
     int in = Collections.frequency(list,element);
     return in;
    }

    public static int binarySearch(ArrayList<Integer> list, Integer key) {
     int in = Collections.binarySearch(list,key);
     return in;
    }
}
