package lvl14.lek4;

import java.util.HashMap;
/**
 * В классе Solution объявлено поле grades типа HashMap<String, Double>, где ключ — имя и фамилия студента,
 * а значение - его средняя оценка. Твоя задача — реализовать метод addStudents, который добавит 5 студентов с
 * их средней оценкой в коллекцию grades.
 * Значения можешь выбрать любые.
 */
public class task1309 {
    public static HashMap<String, Double> grades = new HashMap<>();

    public static void main(String[] args) {
        addStudents();
        System.out.println(grades);
    }

    public static void addStudents() {
        grades.put("Vasya",5.0);
        grades.put("Vaya",4.5);
        grades.put("Vausya",3.3);
        grades.put("Vasoya",3.2);
        grades.put("Vasyja",4.2);
        //напишите тут ваш код
    }
}
