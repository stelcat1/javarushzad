package lvl14.lek4;

import java.util.HashMap;
/**
 * В классе Solution есть метод getProgrammingLanguages, который возвращает список языков программирования.
 * Тебе нужно переписать этот метод, чтобы он возвращал HashMap<Integer, String>.
 * Ключом в этой коллекции будет индекс элемента в ArrayList.
 */
public class task1312 {
    public static void main(String[] args) {
        System.out.println(getProgrammingLanguages());
    }
    public static HashMap<Integer, String> getProgrammingLanguages() {
        //напишите тут ваш код
        HashMap<Integer, String> programmingLanguages = new HashMap<>();
        programmingLanguages.put(1,"Java");
        programmingLanguages.put(2,"Kotlin");
        programmingLanguages.put(3,"Go");
        programmingLanguages.put(4,"Javascript");
        programmingLanguages.put(5,"Typescript");
        programmingLanguages.put(6,"Python");
        programmingLanguages.put(7,"PHP");
        programmingLanguages.put(8,"C++");
        return programmingLanguages;
    }
}
