package lvl14.lek2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

/**
 * В классе Solution есть метод print(HashSet<String>),
 * который должен выводить в консоли все элементы множества, используя iterator().
 * Метод main не участвует в проверке.
 */
public class task1303 {
    public static void print(HashSet<String> words) {
        for (String str : words) {
            System.out.println(str);
        }
        //напишите тут ваш код
    }

    public static void main(String[] args) {
        HashSet<String> words = new HashSet<>(Arrays.asList("Программированию обычно учат на примерах.".split(" ")));
        print(words);
    }
}
