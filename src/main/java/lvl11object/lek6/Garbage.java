package lvl11object.lek6;

public class Garbage {

    private String name;

    public Garbage(String name) {
        this.name = name;
    }

    public Garbage() {
    }

    public static void main(String[] args) throws Throwable {

        for (double i = 0 ; i < 100000000; i++) {

            Garbage garbage = new Garbage();
            garbage = null;//вот здесь первый объект становится доступен сборщику мусора
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Объект garbage уничтожен!");
    }
}