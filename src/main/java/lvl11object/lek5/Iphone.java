package lvl11object.lek5;

import java.util.Objects;

public class Iphone {
    private String model;
    private String color;
    private int price;

    public Iphone(String model, String color, int price) {
        this.model = model;
        this.color = color;
        this.price = price;
    }

    //напишите тут ваш код


    @Override
    public int hashCode() {
        return Objects.hash(model, color, price);
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (!(obj instanceof Iphone)) {
//            return false;
//        }
//
//        Iphone iphone = (Iphone) obj;
//        if ( iphone == null) {
//            return false;
//        }
//        if (!this.model.equals(iphone.model)) {
//            return false;
//        }
//        if (!this.color.equals(iphone.color)) {
//            return false;
//        }
//        return this.price == iphone.price;
//    }

    public static void main(String[] args) {
        Iphone iphone1 = new Iphone("X", "Black", 999);

        Iphone iphone2 = new Iphone("X", "Black", 999);


        System.out.println(iphone1.equals(iphone2));
    }

}