package lvl11object.lek3;

public class Solution {
    public static void main(String[] args) {

        ElectricCar electricCar = new ElectricCar();
        GasCar gasCar = new GasCar();
        HybridCar hybridCar = new HybridCar();

    }

}

class Car {

    Car(String s) {
        System.out.println("Привет. Я " + s);
    }

}

class ElectricCar extends Car {
    ElectricCar() {
        super("ElectricCar");
    }


}

class GasCar extends Car {

    GasCar() {
        super("GasCar");
    }

}

class HybridCar extends Car {
    HybridCar() {
        super("HybridCar");
    }
}


