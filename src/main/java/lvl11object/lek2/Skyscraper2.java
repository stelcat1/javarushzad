package lvl11object.lek2;

public class Skyscraper2 {
    public static final String SKYSCRAPER_WAS_BUILD = "Небоскреб построен.";
    public static final String SKYSCRAPER_WAS_BUILD_FLOORS_COUNT = "Небоскреб построен. Количество этажей - ";
    public static final String SKYSCRAPER_WAS_BUILD_DEVELOPER = "Небоскреб построен. Застройщик - ";

    //напишите тут ваш код

    public int x;
    public String s;

    public Skyscraper2() {

        System.out.println(SKYSCRAPER_WAS_BUILD);

    }

    public Skyscraper2(int x) {

        this.x = x;

        System.out.println(SKYSCRAPER_WAS_BUILD_FLOORS_COUNT + x);

    }

    public Skyscraper2(String s) {

        this.s = s;

        System.out.println(SKYSCRAPER_WAS_BUILD_DEVELOPER + s);

    }

    public static void main(String[] args) {
        Skyscraper2 skyscraper = new Skyscraper2();
        Skyscraper2 skyscraperTower = new Skyscraper2(50);
        Skyscraper2 skyscraperSkyline = new Skyscraper2("JavaRushDevelopment");
    }
}