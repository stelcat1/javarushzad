package lvl11object.lek2;

public class Skyscraper3 {
    private int floorsCount;
    private String developer;

    //напишите тут ваш код

    public Skyscraper3() {

        this.floorsCount = 5;
        this.developer = "JavaRushDevelopment";

    }

    public Skyscraper3(int floorsCount, String developer) {

        this.floorsCount = floorsCount;
        this.developer = developer;

    }

    public static void main(String[] args) {
        Skyscraper3 skyscraper = new Skyscraper3();
        Skyscraper3 skyscraperUnknown = new Skyscraper3(50, "Неизвестно");
    }
}


