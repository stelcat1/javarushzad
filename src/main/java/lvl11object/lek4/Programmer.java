package lvl11object.lek4;

public class Programmer {
    private int salary = 1000;

    public int getSalary() {
        System.out.println(salary);
        return salary;
    }

    public void setSalary(int salary) {
        if (this.salary < salary) {
            this.salary = salary;
        }
    }


}
