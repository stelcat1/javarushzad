package tlssha1;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ParallelSHA1BruteForce {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis(); // Фиксируем время начала работы программы

        // Целевой хэш
        String targetHash = "67f768918c56af41a9756034ab695ebd70a8dbc3";

        // Задаем алфавит символов - русский алфавит (большие и маленькие буквы)
        char[] alphabet = generateRussianAlphabet();

        // Максимальная длина строки, которую мы хотим перебрать
        int maxLength = 20; // Теперь перебираем от 1 до 20 символов

        // Создаем ExecutorService для управления потоками
        ExecutorService executor = Executors.newFixedThreadPool(maxLength);

        // Создаем список для хранения Future
        List<Future<String>> futures = new ArrayList<>();

        // Запускаем задачи для каждой длины от 1 до maxLength
        for (int len = 1; len <= maxLength; len++) {
            Callable<String> task = new BruteForceTask(targetHash, alphabet, len);
            Future<String> future = executor.submit(task);
            futures.add(future);
        }

        // Ожидаем завершения всех задач и определяем время выполнения
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;

        // Ожидаем завершения всех задач
        for (Future<String> future : futures) {
            try {
                String result = future.get();
                if (result != null) {
                    System.out.println("Найдено соответствие:");
                    System.out.println("Исходная строка: " + result);
                    System.out.println("SHA-1 хэш: " + targetHash);
                    break; // Завершаем поиск, если найдено совпадение
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        // Останавливаем ExecutorService
        executor.shutdown();

        // Выводим время выполнения программы
        System.out.println("Время выполнения программы: " + duration + " миллисекунд");
    }

    // Метод для вычисления SHA-1 хэша строки
    public static String sha1(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] digest = md.digest(input.getBytes());
            return bytesToHex(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Вспомогательный метод для конвертации массива байт в строку HEX
    private static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    // Метод для генерации русского алфавита (большие и маленькие буквы)
    private static char[] generateRussianAlphabet() {
        StringBuilder alphabet = new StringBuilder();
        for (char letter = 'а'; letter <= 'я'; letter++) {
            alphabet.append(letter);
        }
        for (char letter = 'А'; letter <= 'Я'; letter++) {
            alphabet.append(letter);
        }
        return alphabet.toString().toCharArray();
    }

    // Класс задачи для подбора строк заданной длины и проверки их хэшей
    private static class BruteForceTask implements Callable<String> {
        private final String targetHash;
        private final char[] alphabet;
        private final int length;

        public BruteForceTask(String targetHash, char[] alphabet, int length) {
            this.targetHash = targetHash;
            this.alphabet = alphabet;
            this.length = length;
        }

        @Override
        public String call() throws Exception {
            char[] currentString = new char[length];
            generateAndCheckStringsRecursive(targetHash, alphabet, currentString, length, 0);
            return null; // Возвращаем null, если совпадение не найдено
        }

        // Рекурсивный метод для генерации и проверки всех строк заданной длины
        private void generateAndCheckStringsRecursive(String targetHash, char[] alphabet, char[] currentString, int length, int index) {
            if (index == length) {
                // Преобразуем текущую строку в строку и вычисляем ее SHA-1 хэш
                String candidate = new String(currentString, 0, length);
                String hashedCandidate = sha1(candidate);

                // Сравниваем полученный хэш с целевым хэшем
                if (hashedCandidate.equals(targetHash)) {
                    System.out.println("Найдено соответствие:");
                    System.out.println("Исходная строка: " + candidate);
                    System.out.println("SHA-1 хэш: " + hashedCandidate);
                    System.exit(0); // Найдя совпадение, завершаем программу
                }
                return;
            }

            // Рекурсивно генерируем все возможные строки заданной длины
            for (int i = 0; i < alphabet.length; i++) {
                currentString[index] = alphabet[i];
                generateAndCheckStringsRecursive(targetHash, alphabet, currentString, length, index + 1);
            }
        }
    }
}
