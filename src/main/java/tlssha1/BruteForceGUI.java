//package tlssha1;
//
//import javafx.application.Application;
//import javafx.geometry.Insets;
//import javafx.scene.Scene;
//import javafx.scene.control.*;
//import javafx.scene.layout.GridPane;
//import javafx.scene.layout.HBox;
//import javafx.stage.Stage;
//import java.awt.*;
//import java.awt.event.*;
//import javax.swing.*;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//public class BruteForceGUI extends Application {
//
//    private TextField targetHashField;
//    private Spinner<Integer> maxLengthSpinner;
//    private TextArea resultArea;
//    private Label timeLabel;
//
//    @Override
//    public void start(Stage primaryStage) {
//        primaryStage.setTitle("SHA-1 Brute Force");
//
//        // Создаем элементы управления
//        targetHashField = new TextField();
//        targetHashField.setPromptText("Введите целевой хэш");
//
//        maxLengthSpinner = new Spinner<>(1, 20, 5); // Минимальное значение, максимальное значение, начальное значение
//        maxLengthSpinner.setEditable(true);
//        maxLengthSpinner.setPrefWidth(70);
//
//        Button startButton = new Button("Запуск");
//        startButton.setOnAction(event -> startBruteForce());
//
//        resultArea = new TextArea();
//        resultArea.setEditable(false);
//        resultArea.setWrapText(true);
//        resultArea.setPrefHeight(200);
//
//        timeLabel = new Label();
//
//        // Создаем сетку для размещения элементов управления
//        GridPane grid = new GridPane();
//        grid.setPadding(new Insets(10, 10, 10, 10));
//        grid.setVgap(10);
//        grid.setHgap(10);
//
//        grid.add(new Label("Целевой хэш:"), 0, 0);
//        grid.add(targetHashField, 1, 0);
//
//        grid.add(new Label("Максимальная длина строки:"), 0, 1);
//        grid.add(maxLengthSpinner, 1, 1);
//
//        HBox buttonBox = new HBox(10);
//        buttonBox.getChildren().add(startButton);
//        grid.add(buttonBox, 0, 2);
//
//        grid.add(new Label("Результат:"), 0, 3);
//        grid.add(resultArea, 0, 4, 2, 1);
//
//        grid.add(new Label("Затраченное время:"), 0, 5);
//        grid.add(timeLabel, 1, 5);
//
//        // Создаем сцену и устанавливаем на основной stage
//        Scene scene = new Scene(grid, 600, 400);
//        primaryStage.setScene(scene);
//        primaryStage.show();
//    }
//
//    // Метод для запуска подбора
//    private void startBruteForce() {
//        String targetHash = targetHashField.getText();
//        int maxLength = maxLengthSpinner.getValue();
//
//        ExecutorService executor = Executors.newSingleThreadExecutor();
//        executor.execute(() -> {
//            long startTime = System.currentTimeMillis();
//            String result = performBruteForce(targetHash, maxLength);
//            long endTime = System.currentTimeMillis();
//            long duration = endTime - startTime;
//
//            // Обновляем интерфейс в JavaFX потоке
//            updateUI(result, duration);
//        });
//        executor.shutdown();
//    }
//
//    // Метод для выполнения подбора
//    private String performBruteForce(String targetHash, int maxLength) {
//        char[] alphabet = generateRussianAlphabet();
//
//        // Перебираем строки от 1 до maxLength
//        for (int len = 1; len <= maxLength; len++) {
//            char[] currentString = new char[len];
//            String result = generateAndCheckStrings(targetHash, alphabet, currentString, len);
//            if (result != null) {
//                return result;
//            }
//        }
//        return null;
//    }
//
//    // Метод для генерации и проверки всех строк заданной длины
//    private String generateAndCheckStrings(String targetHash, char[] alphabet, char[] currentString, int length) {
//        return generateAndCheckStringsRecursive(targetHash, alphabet, currentString, length, 0);
//    }
//
//    // Рекурсивный метод для генерации и проверки всех строк заданной длины
//    private String generateAndCheckStringsRecursive(String targetHash, char[] alphabet, char[] currentString, int length, int index) {
//        if (index == length) {
//            // Преобразуем текущую строку в строку и вычисляем ее SHA-1 хэш
//            String candidate = new String(currentString, 0, length);
//            String hashedCandidate = sha1(candidate);
//
//            // Сравниваем полученный хэш с целевым хэшем
//            if (hashedCandidate.equals(targetHash)) {
//                return candidate;
//            }
//            return null;
//        }
//
//        // Рекурсивно генерируем все возможные строки заданной длины
//        for (int i = 0; i < alphabet.length; i++) {
//            currentString[index] = alphabet[i];
//            String result = generateAndCheckStringsRecursive(targetHash, alphabet, currentString, length, index + 1);
//            if (result != null) {
//                return result;
//            }
//        }
//        return null;
//    }
//
//    // Метод для вычисления SHA-1 хэша строки
//    private String sha1(String input) {
//        try {
//            MessageDigest md = MessageDigest.getInstance("SHA-1");
//            byte[] digest = md.digest(input.getBytes());
//            return bytesToHex(digest);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    // Вспомогательный метод для конвертации массива байт в строку HEX
//    private String bytesToHex(byte[] bytes) {
//        StringBuilder sb = new StringBuilder();
//        for (byte b : bytes) {
//            sb.append(String.format("%02x", b));
//        }
//        return sb.toString();
//    }
//
//    // Метод для генерации русского алфавита (большие и маленькие буквы)
//    private char[] generateRussianAlphabet() {
//        StringBuilder alphabet = new StringBuilder();
//        for (char letter = 'а'; letter <= 'я'; letter++) {
//            alphabet.append(letter);
//        }
//        for (char letter = 'А'; letter <= 'Я'; letter++) {
//            alphabet.append(letter);
//        }
//        return alphabet.toString().toCharArray();
//    }
//
//    // Метод для обновления интерфейса после выполнения подбора
//    private void updateUI(String result, long duration) {
//        // Обновляем интерфейс в JavaFX потоке
//        javafx.application.Platform.runLater(() -> {
//            if (result != null) {
//                resultArea.setText("Найдено соответствие:\nИсходная строка: " + result);
//            } else {
//                resultArea.setText("Совпадение не найдено.");
//            }
//            timeLabel.setText(duration + " миллисекунд");
//        });
//    }
//
//    public static void main(String[] args) {
//        launch(args);
//    }
//}
