package lvl13.lek5.Solution2;

import java.util.ArrayList;
import java.util.Arrays;

/*

В методе main найди и удали язык программирования Pascal из списка programmingLanguages.

Требования:
•	В методе main должен удаляться Pascal из списка programmingLanguages.
 */
public class Solution_remove {
    public static ArrayList<String> programmingLanguages = new ArrayList<>(Arrays.asList("C", "C++", "Python", "JavaScript", "Ruby", "Java", "Pascal"));

    public static void main(String[] args) {
        //напишите тут ваш код
        programmingLanguages.remove("Python");
        for (int i = 0; i < programmingLanguages.size(); i++) {
            System.out.println(String.format("%s — %d-я планета от Солнца", programmingLanguages.get(i), (i + 1)));
        }
        System.out.println();
    }
}
