package org.example.Stringbuilderrr;

/*
Поработаем со StringBuilder
*/

import java.util.Arrays;

public class strbuilder1 {
    public static void main(String[] args) {
        String string = "Учиться, учиться и еще раз учиться! ";

        System.out.println(addTo(string, new String[]{"Под ", "лежачий ", "камень ", "вода ", "не ", "течет"}));
        System.out.println(replace(string, ", ", 16, 27));
    }

    public static StringBuilder addTo(String string, String[] strings) {
        StringBuilder itog = new StringBuilder(string);
        for (int j = 0; j < strings.length; j++) {
            itog.append(strings[j]);
        }
        return itog;
    }

    public static StringBuilder replace(String string, String str, int start, int end) {
        StringBuilder itog = new StringBuilder(string);
        return itog.replace(start, end, str);
    }
}

