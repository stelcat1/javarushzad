package org.example;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SnakeGame extends JFrame implements ActionListener {
    private static final int WIDTH = 800;
    private static final int HEIGHT = 600;
    private static final int DOT_SIZE = 20;
    private static final int ALL_DOTS = (WIDTH * HEIGHT) / (DOT_SIZE * DOT_SIZE);
    private static final int RAND_POS = 30;
    private static final int DELAY = 150;

    private final int[] x = new int[ALL_DOTS];
    private final int[] y = new int[ALL_DOTS];
    private int dots;
    private int appleX;
    private int appleY;
    private boolean inGame = true;
    private Timer timer;

    public SnakeGame() {
        setTitle("Snake Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setResizable(false);
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.BLACK);
        setLayout(null);

        JButton restartButton = new JButton("Restart");
        restartButton.setBounds(350, 10, 100, 30);
        restartButton.addActionListener(this);
        add(restartButton);

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                if ((key == KeyEvent.VK_LEFT) && (!Direction.RIGHT.equals(Direction.getCurrentDirection()))) {
                    Direction.setCurrentDirection(Direction.LEFT);
                } else if ((key == KeyEvent.VK_RIGHT) && (!Direction.LEFT.equals(Direction.getCurrentDirection()))) {
                    Direction.setCurrentDirection(Direction.RIGHT);
                } else if ((key == KeyEvent.VK_UP) && (!Direction.DOWN.equals(Direction.getCurrentDirection()))) {
                    Direction.setCurrentDirection(Direction.UP);
                } else if ((key == KeyEvent.VK_DOWN) && (!Direction.UP.equals(Direction.getCurrentDirection()))) {
                    Direction.setCurrentDirection(Direction.DOWN);
                }
            }
        });

        initGame();
    }

    private void initGame() {
        dots = 3;

        for (int i = 0; i < dots; i++) {
            x[i] = 50 - i * DOT_SIZE;
            y[i] = 50;
        }

        locateApple();

        timer = new Timer(DELAY, this);
        timer.start();
    }

    private void locateApple() {
        int r = (int) (Math.random() * RAND_POS);
        appleX = r * DOT_SIZE;

        r = (int) (Math.random() * RAND_POS);
        appleY = r * DOT_SIZE;
    }

    private void checkApple() {
        if ((x[0] == appleX) && (y[0] == appleY)) {
            dots++;
            locateApple();
        }
    }

    private void checkCollision() {
        for (int i = dots; i > 0; i--) {
            if ((i > 4) && (x[0] == x[i]) && (y[0] == y[i])) {
                inGame = false;
            }
        }

        if (y[0] >= HEIGHT) {
            inGame = false;
        }

        if (y[0] < 0) {
            inGame = false;
        }

        if (x[0] >= WIDTH) {
            inGame = false;
        }

        if (x[0] < 0) {
            inGame = false;
        }

        if (!inGame) {
            timer.stop();
        }
    }

    private void move() {
        for (int i = dots; i > 0; i--) {
            x[i] = x[i - 1];
            y[i] = y[i - 1];
        }

        if (Direction.LEFT.equals(Direction.getCurrentDirection())) {
            x[0] -= DOT_SIZE;
        }

        if (Direction.RIGHT.equals(Direction.getCurrentDirection())) {
            x[0] += DOT_SIZE;
        }

        if (Direction.UP.equals(Direction.getCurrentDirection())) {
            y[0] -= DOT_SIZE;
        }

        if (Direction.DOWN.equals(Direction.getCurrentDirection())) {
            y[0] += DOT_SIZE;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (inGame) {
            checkApple();
            checkCollision();
            move();
        }

        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        if (inGame) {
            g.setColor(Color.RED);
            g.fillOval(appleX, appleY, DOT_SIZE, DOT_SIZE);

            for (int i = 0; i < dots; i++) {
                g.setColor(Color.GREEN);
                g.fillRect(x[i], y[i], DOT_SIZE, DOT_SIZE);
            }

            Toolkit.getDefaultToolkit().sync();
        } else {
            gameOver(g);
        }
    }

    private void gameOver(Graphics g) {
        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 40);
        FontMetrics metrics = getFontMetrics(font);

        g.setColor(Color.WHITE);
        g.setFont(font);
        g.drawString(message, (WIDTH - metrics.stringWidth(message)) / 2, HEIGHT / 2);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            SnakeGame game = new SnakeGame();
            game.setVisible(true);
        });
    }
}

class Direction {
    public static final String LEFT = "left";
    public static final String RIGHT = "right";
    public static final String UP = "up";
    public static final String DOWN = "down";

    private static String currentDirection = RIGHT;

    public static String getCurrentDirection() {
        return currentDirection;
    }

    public static void setCurrentDirection(String direction) {
        currentDirection = direction;
    }
}