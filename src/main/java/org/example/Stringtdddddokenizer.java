package org.example;

import java.util.Arrays;
import java.util.StringTokenizer;
/*
StringTokenizer
*/

public class Stringtdddddokenizer {
    public static void main(String[] args) {
        String packagePath = "java.util.stream";
        String[] tokens = getTokens(packagePath, "\\.");
        System.out.println(Arrays.toString(tokens));
    }

    public static String[] getTokens(String query, String delimiter) {
        StringTokenizer tokenizer = new StringTokenizer(query, delimiter);
        String[] token = new String[tokenizer.countTokens()];
        int i = 0;
        while (tokenizer.hasMoreTokens()) {
            token[i++] = tokenizer.nextToken();
        }
        return token;
    }
}
