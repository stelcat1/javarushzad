package org.example;

/*
String pool
*/

public class stringpoool {
    public static void main(String[] args) {
        String first = new String("JavaRush");
        String second = new String("JavaRush");
        String third = new String("javarush");
        System.out.println(equal(first, second));
        System.out.println(equal(second, third));
    }

    public static boolean equal(String first, String second) {
        String firstI = first.intern();
        String secondI = second.intern();
        return firstI == secondI;

    }
}
